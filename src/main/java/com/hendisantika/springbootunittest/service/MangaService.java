package com.hendisantika.springbootunittest.service;

import com.hendisantika.springbootunittest.domain.Manga;
import com.hendisantika.springbootunittest.domain.MangaResult;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.util.List;
import java.util.Objects;

/**
 * Created by IntelliJ IDEA.
 * Project : springboot-unit-test
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 2019-02-05
 * Time: 11:39
 */
@Service
public class MangaService {

    private static final String MANGA_SEARCH_URL = "http://api.jikan.moe/search/manga/";
    Logger logger = LoggerFactory.getLogger(MangaService.class);
    @Autowired
    RestTemplate restTemplate;


    public List<Manga> getMangasByTitle(String title) {
        return Objects.requireNonNull(restTemplate.getForEntity(MANGA_SEARCH_URL + title, MangaResult.class).getBody()).getResult();
    }

}
