package com.hendisantika.springbootunittest.domain;

import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

/**
 * Created by IntelliJ IDEA.
 * Project : springboot-unit-test
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 2019-02-05
 * Time: 11:38
 */
@Data
@NoArgsConstructor
public class MangaResult {

    private List<Manga> result;

}