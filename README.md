# springboot-unit-test

This project shows how to do unit and integration tests in a spring boot environment.

The demo system is a Manga search engine, that allows searching by title.

#### Requirements
For building and running the application you need:

1. JDK 1.8
2. Maven 3

#### Running the application locally
You can use the [Spring Boot Maven plugin](https://docs.spring.io/spring-boot/docs/current/reference/html/build-tool-plugins-maven-plugin.html) like so:

```
mvn spring-boot:run
```